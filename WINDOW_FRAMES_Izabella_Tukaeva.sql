SALES REPORT

| week_number | day_of_week | amount | CUM_SUM | CENTERED_3_DAY_AVG |
|-------------|-------------|--------|---------|--------------------|
| 49          | Monday      | 100    | 100     | 75                 |
| 49          | Tuesday     | 120    | 220     | 110                |
| 49          | Wednesday   | 80     | 300     | 100                |
| 49          | Thursday    | 150    | 450     | 117.5              |
| 49          | Friday      | 200    | 650     | 116.6667           |
| 49          | Saturday    | 180    | 830     | 133.3333           |
| 49          | Sunday      | 90     | 920     | 100                |
| 50          | Monday      | 110    | 1030    | 93.3333            |
| 50          | ...         | ...    | ...     | ...                |
| 51          | Sunday      | 120    | ...     | ...                |


